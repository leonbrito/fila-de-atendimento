require 'test_helper'

class ListaConsultasControllerTest < ActionController::TestCase
  setup do
    @lista_consulta = lista_consultas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lista_consultas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lista_consulta" do
    assert_difference('ListaConsulta.count') do
      post :create, lista_consulta: { dia: @lista_consulta.dia }
    end

    assert_redirected_to lista_consulta_path(assigns(:lista_consulta))
  end

  test "should show lista_consulta" do
    get :show, id: @lista_consulta
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lista_consulta
    assert_response :success
  end

  test "should update lista_consulta" do
    patch :update, id: @lista_consulta, lista_consulta: { dia: @lista_consulta.dia }
    assert_redirected_to lista_consulta_path(assigns(:lista_consulta))
  end

  test "should destroy lista_consulta" do
    assert_difference('ListaConsulta.count', -1) do
      delete :destroy, id: @lista_consulta
    end

    assert_redirected_to lista_consultas_path
  end
end
