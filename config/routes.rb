Rails.application.routes.draw do
  resources :lista_consultas
  resources :consultas
  resources :tipo_consultas
  resources :paciente_consultas, only: [:index, :destroy]
  #resources :prioridades
  resources :pacientes
  resources :usuarios
  resources :sessao_usuarios, only: [:create, :new, :destroy]
  resources :sessao_pacientes, only: [:create, :login, :destroy]

  get 'pacientes/lista' => 'pacientes#index'
  match '/sessao_pacientes/loginComHashCPF' => 'sessao_pacientes#loginComHashCPF', via: [:get, :post]
  match '/sessao_pacientes' => 'sessao_pacientes#login', via: [:get, :post]
  match '/paciente_consultas/getFila' => 'paciente_consultas#getFila', via: [:get, :post]
  match '/paciente_consultas/getData' => 'paciente_consultas#getData', via: [:get, :post]
  match '/atendimento' => 'lista_consultas#atendimento', via: [:get, :post]
  match '/chamar' => 'consultas#chamar', via: [:get, :post]
  match '/dispensar' => 'consultas#dispensar', via: [:get, :post]
  match '/desmarcar' => 'consultas#desmarcar', via: [:get, :post]
  match '/atualizar' => 'paciente_consultas#atualizar', via:[:get,:post]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'sessao_usuarios#new'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
