class CreatePacientes < ActiveRecord::Migration
  def change
    create_table :pacientes do |t|
      t.string :nome
      t.string :cpf
      t.string :hashcpf
      t.column :prioridade, :integer, default: 0, null: false
      t.string :telefone

      t.timestamps null: false
    end
  end
end
