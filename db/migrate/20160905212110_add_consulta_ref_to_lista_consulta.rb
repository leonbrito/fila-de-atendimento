class AddConsultaRefToListaConsulta < ActiveRecord::Migration
  def change
    add_reference :lista_consultas, :consulta, index: true, foreign_key: true
  end
end
