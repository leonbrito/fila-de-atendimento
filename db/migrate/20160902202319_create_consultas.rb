class CreateConsultas < ActiveRecord::Migration
  def change
    create_table :consultas do |t|
      t.time :inicio
      t.time :fim
      t.time :duracao
      t.date :data
      t.column :status, :integer, default: 0, null: false
      t.integer :numeroFila
      t.string :tipo_consulta
      t.string :paciente_nome

      t.timestamps null: false
    end
  end
end
