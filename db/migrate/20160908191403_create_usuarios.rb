class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.string :password_digest
      t.column :nivel, :integer, default: 0, null: false
      t.timestamps null: false
    end
  end
end
