# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160908191403) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "consultas", force: :cascade do |t|
    t.time     "inicio"
    t.time     "fim"
    t.time     "duracao"
    t.date     "data"
    t.integer  "status",           default: 0, null: false
    t.integer  "numeroFila"
    t.string   "tipo_consulta"
    t.string   "paciente_nome"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "paciente_id"
    t.integer  "tipo_consulta_id"
  end

  add_index "consultas", ["paciente_id"], name: "index_consultas_on_paciente_id", using: :btree
  add_index "consultas", ["tipo_consulta_id"], name: "index_consultas_on_tipo_consulta_id", using: :btree

  create_table "lista_consultas", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "consulta_id"
  end

  add_index "lista_consultas", ["consulta_id"], name: "index_lista_consultas_on_consulta_id", using: :btree

  create_table "pacientes", force: :cascade do |t|
    t.string   "nome"
    t.string   "cpf"
    t.string   "hashcpf"
    t.integer  "prioridade", default: 0, null: false
    t.string   "telefone"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tipo_consultas", force: :cascade do |t|
    t.string   "nome"
    t.time     "duracao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "nome"
    t.string   "password_digest"
    t.integer  "nivel",           default: 0, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_foreign_key "consultas", "pacientes"
  add_foreign_key "consultas", "tipo_consultas"
  add_foreign_key "lista_consultas", "consultas"
end
