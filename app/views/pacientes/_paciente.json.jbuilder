json.extract! paciente, :id, :nome, :cpf, :prioridade, :hashcpf, :telefone, :created_at, :updated_at
json.url paciente_url(paciente, format: :json)