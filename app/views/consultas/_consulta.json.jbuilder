json.extract! consulta, :id, :inicio, :fim, :duracao, :data, :tipo_consulta, :status, :numeroFila, :paciente_id, :created_at, :updated_at
json.url consulta_url(consulta, format: :json)