json.extract! prioridade, :id, :nome, :created_at, :updated_at
json.url prioridade_url(prioridade, format: :json)