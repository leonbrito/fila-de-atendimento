class Usuario < ActiveRecord::Base
	enum nivel: {medico: 0, auxiliar: 1}
	validates_presence_of :nome
	has_secure_password
	scope :most_recent, -> { order('created_at DESC') }
	
    def self.authenticate(nome, password)
        usuario = find_by(nome: nome).
			try(:authenticate, password)
    end

end
