class ListaConsulta < ActiveRecord::Base
	validates :dia, presence: true
	has_many :consultas

	  # Retorna um json contendo as datas da consulta do usuário...
  def self.getData(id)
  	@datas = ListaConsultasHelper.getDatas(id)
  	# Remove as datas repetidas
  	@datas = @datas.select(:data).distinct
    # Converte a data para o formato PtBr
	# @datas.order(:data)
	@datas = @datas.sort_by { |d| d.data }
    @dt = []
    @datas.each { |d| @dt << {:data => d.data.strftime("%d-%m-%Y")}}
    render :json => @dt
  end
end
