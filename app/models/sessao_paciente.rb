class SessaoPaciente
	include ActiveModel::Model

	attr_accessor :cpf
    validates_presence_of :cpf

    def initialize(sessao, atributos={})
		@session = sessao
		@cpf = atributos[:cpf]
	end

	def authenticate
		paciente = Paciente.authenticate(@cpf)
		if paciente.present?
			guarda(paciente)
		else
			errors.add(:base, :invalid_login)
			false
		end
	end

	def buscanalista(nome)
		@data = Date.current
	  	@consultas = ListaConsultasHelper.getDate(@data)
		
		@consultas.each do |f|
			if f.paciente_nome == nome
				return true
			end
		end
		return false
	end

	def paciente_atual
		Paciente.find(@session[:paciente_id])
	end

	def guarda(paciente)
        @session[:paciente_id] = paciente.id
    end

    def paciente_logado?
        @session[:paciente_id].present?
    end

    def deleta_sessao_paciente
        @session[:paciente_id] = nil
    end
end