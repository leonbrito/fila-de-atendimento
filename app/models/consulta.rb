class Consulta < ActiveRecord::Base
	enum status: {espera: 0, atendimento: 1, atendido: 2, cancelado: 3}
	scope :most_recent, -> { order('created_at DESC') }
	validates :inicio, :data, presence: true
	validates :paciente_nome , presence: {message: "não pode ficar em branco, faça uma busca!"}
	has_many :tipo_consultas
	has_one :pacientes
	belongs_to :lista_consulta
	paginates_per 5
end