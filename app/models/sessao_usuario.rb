class SessaoUsuario
    include ActiveModel::Model

    attr_accessor :nome, :password
    validates_presence_of :nome, :password

    def initialize(sessao, atributos={})
        @session = sessao
        @nome = atributos[:nome]
        @password = atributos[:password]
    end

    def authenticate!
        usuario = Usuario.authenticate(@nome, @password)
        if usuario.present?
            store(usuario)
        else
            errors.add(:base, :invalid_login)
            false
        end

    end

    def store(usuario)
        @session[:usuario_id] = usuario.id
    end

    def usuario_atual
        Usuario.find(@session[:usuario_id])
    end

    def logado?
        @session[:usuario_id].present?
    end

    def destroy
        @session[:usuario_id] = nil
    end
end