class PacienteConsulta < ActiveRecord::Base

	def self.getData(data,id)	
		@datas = ListaConsultasHelper.getDatas(params[:id])
  		# Remove as datas repetidas
  		@datas = @datas.select(:data).distinct
    	# Converte a data para o formato PtBr
		# @datas.order(:data)
	  	@datas = @datas.sort_by { |d| d.data }
    	@dt = []
    	@datas.each { |d| @dt << {:data => d.data.strftime("%d-%m-%Y")}}
    	return @dt
	end

	def self.getFila(data,id)
  		fila = []	
		@dta = data
		@dta = @dta.split("-")
		@dta = "#{@dta[2]}-#{@dta[1]}-#{@dta[0]}"
  		@consultas = ListaConsultasHelper.getDate(@dta)
  		@p = Paciente.find(id)
		@count = 1 #Contador de Posições
  		
  		# Remove quem já foi atendido e quem cancelou...
  		@consultas = @consultas.where.not(status: 2).where.not(status: 3)
    	if(@consultas.find_by(paciente_id: id))
      		@first = @consultas.find_by(status: 1)
      		@consultas = @consultas.where.not(status: 1).order(:inicio)

      		if @first != nil
				p = Paciente.find_by(id: @first.paciente_id)

        	if p.id == id.to_i
          		fila << {:paciente_nome => p.nome, :inicio => @first.inicio.strftime("%H:%M"),
					 :status => @first.status, :numeroFila => @first.numeroFila, :posicao => @count, :prioridade => p.prioridade}
        	else
          		fila << {:paciente_nome => "Anônimo", :inicio => @first.inicio.strftime("%H:%M"),
		   			:status => @first.status, :numeroFila => @first.numeroFila, :posicao => @count, :prioridade => p.prioridade}
        	end
			@count = @count + 1
      	end
						
     	# Crias um array com os parâmetros que preciso e renomeia os anônimos
     		@consultas.each do |f|
				p = Paciente.find_by(id: f.paciente_id)
        		
        		if p.id == id.to_i
   		   			fila << {:paciente_nome => p.nome, :inicio => f.inicio.strftime("%H:%M"),
			   		:status => f.status, :numeroFila => f.numeroFila, :posicao=> @count, :prioridade => p.prioridade}
    			else
   		    		fila << {:paciente_nome => "Anônimo", :inicio => f.inicio.strftime("%H:%M"),
			   		:status => f.status, :numeroFila => f.numeroFila, :posicao => @count, :prioridade => p.prioridade}
       			end
				@count = @count + 1
      		end
    	end
    	return fila
  	end
end