class Paciente < ActiveRecord::Base
	enum prioridade: {normal: 0, idoso: 1, gestante: 2, deficiente: 3, crianca_de_colo: 4}
	scope :most_recent, -> { order('created_at DESC') }
    validates :nome, :cpf, presence: true
	validates :cpf, uniqueness: {message: "já exite!"}, 
					numericality: {message: 'deve ser um número'},
					length: { is: 11 } 
	belongs_to :consulta
	paginates_per 5

	# @@base_uri = 'https://teste-fcda1.firebaseio.com/'
	# @@secret_key = 'AIzaSyCuJAiJ_3oQ0C7oxTUJKSkYnp-JQtft-Ew'
	# @@to = "eqFTAv40ITI:APA91bFYASO3Nq1K-1la8QSoWD-Iipxvd_Pk1KbWht3LqRVAgj5nFEuyjo3cCvPww252AUQd2yvEkUuUxeNgnX8iID1TennTd4aBhfAX8ePmIInk43fw6WKy6C6ICU2lXYNoig0BUhjc"

	def self.search(query)
  		if query.present?
  			find_by(['nome LIKE :query OR
  				cpf LIKE :query', query: "#{query}"])
  		end
	end

	def self.authenticate(cpf)
		cpf = cpf.tr('.','')
		cpf = cpf.tr('-','')
		hashcpf = Digest::SHA256.hexdigest cpf
		paciente = find_by(hashcpf: hashcpf)
    end

	# def fireBaseNotify
	# 	firebase = Firebase::Client.new(base_uri, secret_key)
	# 	response = firebase.push(
	# 		"notification" => { 
	# 			:body => "Pick the milk",
	# 			:title => "Fila de Atendimento",
	# 			:sound => "default",
	# 			:priority => "high" 
	# 		}, "data" => {:id => 9} , 
	# 		"to"=> Paciente.to)
	# end
end