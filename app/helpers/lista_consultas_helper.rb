module ListaConsultasHelper
	# Busca as consultas do DatePicker
	def self.getDate(date)
		if(date != nil)
			Consulta.where(data: date).order(:status)
		end
	end

	def getDate(date)
		ListaConsultasHelper.getDate(date)
	end

	# Busca consultas apartir do dia atual e com o id certo
	def self.getDatas(id)
		Consulta.where("data >= ?  and (status = ? or status = ?)  and paciente_id = ?", 
			Date.current, 0, 1, id)
	end

	def getDatas(id)
		ListaConsultasHelper.getDatas(id)
	end
end
