module ConsultasHelper
	# Busca nome do paciente para colocar na consulta
	def self.getIdPaciente(n)
		if(n != "")
			@p = Paciente.find_by(nome: n)
			@p.id
		end
	end

	def getIdPaciente(n)
		ConsultasHelper(n)
	end

	# Busca o nome do tipo da consulta
	def self.getNomeTipoConsulta(tc)
		@p = TipoConsulta.find_by(id: tc)
		@p.nome
	end

	def getNomeTipoConsulta(tc)
		ConsultasHelper(tc)
	end

	# Busca a duração do tipo da consulta
	def self.getDuracaoConsulta(dtc)
		@p = TipoConsulta.find_by(id: dtc)
		@p.duracao
	end

	def getDuracaoConsulta(tc)
		ConsultasHelper(tc)
	end

	# Busca a duração do tipo da consulta
	def self.getFimConsulta(c)
		f = c.inicio
		f + c.duracao.strftime("%H").to_i.hour + c.duracao.strftime("%M").to_i.minutes
	end

	def getFimConsulta(c)
		ConsultasHelper(c)
	end


	# Gera número fila
	def self.getNumFila()
		if Consulta.last != nil
			Consulta.last.numeroFila + 1000
		else
			1000
		end
	end

	def getNumFila()
		ConsultasHelper()
	end

	# Gera número fila
	def self.calcularMedia(tc)
		at = Consulta.where(data: Date.current, tipo_consulta: tc, status: 2)
		if at.size != 0
			minutos = 0
			at.each do |t|
				k = (t.fim.min - t.inicio.min)
				if k < 0
					k = k + 60
				end
				minutos =  minutos + k
			end
			hora = 0
			minutos = minutos/at.size
			if(minutos > 60)
				hora = minutos/60
				minutos = minutos % 60
			end
			if(minutos < 0)
				minutos = 0
			end
			t = TipoConsulta.find_by(nome: tc)
			t.duracao = Time.new(2000,01,01,hora,minutos,0, "-00:00")
			t.save
			rest = Consulta.where(data: Date.current, tipo_consulta: tc, status: 1)
			rest.each do |r|
				r.duracao = Time.new(2000,01,01,hora,minutos,0, "-00:00")
				r.save
			end
		end
	end

	def calcularMedia(tc)
		ConsultasHelper(tc)
	end

	def self.atualizarHorarios()
		es = Consulta.where(data: Date.current, status: 0).order(:inicio)
		at = Consulta.find_by(status: 1)
		duracao = Time.new(2001,01,01,00,00,00)
		if es.size != 0
			es.each do |e|
				e.inicio = at.fim + duracao.strftime("%H").to_i.hour + duracao.strftime("%M").to_i.minutes
				e.fim = getFimConsulta(e)
				e.save
				duracao = duracao + e.duracao.strftime("%H").to_i.hour + e.duracao.strftime("%M").to_i.minutes
			end
		end
	end

	def atualizarHorarios()
		ConsultasHelper()
	end
end
