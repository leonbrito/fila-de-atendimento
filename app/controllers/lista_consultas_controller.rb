class ListaConsultasController < ApplicationController
  before_action :autenticacao_requerida
  before_action :nivel_medico, only: [:atendimento]
  before_action :nivel_auxiliar, except: [:atendimento, :getData, :getFila]
  before_action :set_lista_consulta, only: [:show, :edit, :update, :destroy]
  protect_from_forgery with: :null_session

  # GET /lista_consultas
  # GET /lista_consultas.json
  def index
  	@data = params[:date]
  	if @data != nil
			# Transforma string no tipo tempo...
			@data = @data.to_time
  		@consultas = ListaConsultasHelper.getDate(@data)
  	else
  		@data = Date.current
  		@consultas = ListaConsultasHelper.getDate(@data)
  	end
  	if (@consultas != nil)
  		@total = @consultas.size
  	end
  	@espera = @consultas.where(status: 0).order(:inicio)
  	@atendimento = @consultas.find_by(status: 1)
  	@atendido = @consultas.where(status: 2)
  	@cancelado = @consultas.where(status: 3)
		@count = 1
  end

  def atendimento
	  @data = Date.current
	  @consultas = ListaConsultasHelper.getDate(@data)
	  @total = @consultas.size
	  @espera = @consultas.where(status: 0).order(:inicio)
	  @atendimento = @consultas.find_by(status: 1)
	  @atendido = @consultas.where(status: 2)
	  @cancelado = @consultas.where(status: 3)
  end
end
