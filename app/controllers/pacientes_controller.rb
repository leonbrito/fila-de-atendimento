class PacientesController < ApplicationController
  before_action :autenticacao_requerida, except: [:getId]
  before_action :nivel_auxiliar, except: [:getId]
  before_action :set_paciente, only: [:show, :edit, :update, :destroy]

  # GET /pacientes
  # GET /pacientes.json
  def index
    @info = params[:info]
    if(@info == nil)
	    @pacientes = Paciente.most_recent.page params[:page]
    else
       @pacientes = Paciente.where('nome like ? OR cpf like ?', "%#{@info}%", "%#{@info}%").most_recent.page params[:page]
    end

  end

  # GET /pacientes/1
  # GET /pacientes/1.json
  def show
	 @paciente = Paciente.find(params[:id])
  end

  # GET /pacientes/new
  def new
    @paciente = Paciente.new
  end

  # GET /pacien/1/edit
  def edit
  end

  # POST /pacientes
  # POST /pacientes.json
  def create
    @paciente = Paciente.new(paciente_params)
    @paciente.hashcpf = Digest::SHA256.hexdigest @paciente.cpf
    respond_to do |format|
      if @paciente.save
        format.html { redirect_to @paciente, notice: 'Paciente foi criado com sucesso.' }
        format.json { render :show, status: :created, location: @paciente }
      else
        format.html { render :new }
        format.json { render json: @paciente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pacientes/1
  # PATCH/PUT /pacientes/1.json
  def update
    respond_to do |format|
      if @paciente.update(paciente_params)
        @paciente.hashcpf = Digest::SHA256.hexdigest @paciente.cpf
        @paciente.save
        format.html { redirect_to @paciente, notice: 'Paciente foi atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @paciente }
      else
        format.html { render :edit }
        format.json { render json: @paciente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pacientes/1
  # DELETE /pacientes/1.json
  def destroy
	@consultas = Consulta.where(paciente_id: params[:id])
	@consultas.each do |c|
		c.destroy
		c.save
	end
    @paciente.destroy
    respond_to do |format|
      format.html { redirect_to pacientes_url, notice: 'Paciente apagado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paciente
      @paciente = Paciente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paciente_params
      params.require(:paciente).permit(:nome, :cpf, :prioridade, :hashcpf, :telefone)
	end
end
