class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  delegate :usuario_atual, :logado?, to: :sessao_usuario
  helper_method :usuario_atual, :logado?

  delegate :paciente_atual, :paciente_logado?, to: :sessao_paciente
  helper_method :paciente_atual, :paciente_logado?, :atualizar

  def sessao_usuario
    SessaoUsuario.new(session)
  end

  def sessao_paciente
    SessaoPaciente.new(session)
  end

  def autenticacao_requerida
    unless logado?
      redirect_to new_sessao_usuario_path
    end
  end

  def autenticacao_requerida_paciente
    unless paciente_logado?
      redirect_to new_sessao_paciente_path
    end
  end

  def nivel_medico
    unless usuario_atual.nivel == "medico"
      redirect_to  lista_consultas_path 
    end
  end

  def nivel_auxiliar
    unless usuario_atual.nivel == "auxiliar"
      redirect_to  atendimento_path
    end
  end
end