class SessaoPacientesController < ApplicationController
	def login
		if (!paciente_logado?) 
        	@sessaopaciente = SessaoPaciente.new(session)
        else
        	redirect_to paciente_consultas_path
        end
    end

    def create
        @sessaopaciente = SessaoPaciente.new(session, params[:sessao_paciente])
		if (@sessaopaciente.authenticate)
			if (@sessaopaciente.buscanalista(paciente_atual.nome))
				redirect_to paciente_consultas_path, notice: "Logado, Bem Vindo  #{paciente_atual.nome}"
			else
                sessao_paciente.deleta_sessao_paciente
				redirect_to sessao_pacientes_path, notice: "Paciente não está na fila de espera."
			end
		else
			render :login, notice: "Paciente não cadastrado."
		end
    end

    def loginComHashCPF
        @hashcpf = params[:hashcpf]
        @p = Paciente.find_by(hashcpf: @hashcpf)
        render :json => @p.as_json(only: [:id])
    end

    def destroy
        sessao_paciente.deleta_sessao_paciente
        redirect_to sessao_pacientes_path, notice: "Logout efetuado"
    end
end