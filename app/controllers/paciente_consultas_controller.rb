class PacienteConsultasController < ApplicationController
	before_action :autenticacao_requerida_paciente, except: [:getData,:getFila,:getFilaIndex]
	protect_from_forgery with: :null_session

	def index
		@data = Date.today
		@data = @data.strftime("%d-%m-%Y")
		@id = paciente_atual.id
	  	@consultas_array = getFilaIndex(@data,@id)
	  	@consultas = Kaminari.paginate_array(@consultas_array)		
	end

	def atualizar
		paciente_consultas_path
	end

	  # Retorna um json contendo as datas da consulta do usuário...
  	def getData
  		@dt = PacienteConsulta.getData(params[:date], params[:id])
    	render :json => @dt
  	end

  	def getFila
  		fila = getFilaIndex(params[:date],params[:id])
  		render :json => fila
  	end

  private
  	def getFilaIndex(data,id)
    	return PacienteConsulta.getFila(data,id)
  	end
end