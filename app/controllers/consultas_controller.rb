class ConsultasController < ApplicationController
  before_action :autenticacao_requerida
  before_action :nivel_auxiliar, except: [:chamar, :dispensar]
  before_action :set_consulta, only: [:show, :edit, :update, :destroy]

  # GET /consultas
  # GET /consultas.json
  def index
    if (paciente_logado?)
      redirect_to paciente_consultas_path
    end

    @info = params[:info]
    if(@info == nil)
      @consultas = Consulta.most_recent.page params[:page]
    else
      @pacientes = Paciente.where('nome like ? OR cpf like ?', "%#{@info}%", "%#{@info}%")
      puts @pacientes
      @consultas = Consulta.where(paciente_id: @pacientes.each { |p| p.id}).most_recent.page params[:page]
    end
  end

  def chamar
	  @c = Consulta.find(params[:consulta])
	  @atendimento = Consulta.find_by(status: 1)
	  @tempo = Time.now - 3.hour
	  if (@atendimento != nil)
		  @atendimento.atendido!
		  @atendimento.fim = @tempo
		  @atendimento.save
	  end
    if @c.status == 'espera'
      @c.inicio = @tempo
      @c.fim = ConsultasHelper.getFimConsulta(@c)
      @c.atendimento!
      ConsultasHelper.atualizarHorarios
      ConsultasHelper.calcularMedia(@c.tipo_consulta)
      redirect_to atendimento_path
    else
      redirect_to atendimento_path, notice: 'Esse paciente não está mais em espera.'
    end
  end

  def dispensar
	  @c = Consulta.find(params[:consulta])

	  @c.atendido!
	  @c.fim = Time.now - 3.hour
	  @c.save
	  ConsultasHelper.calcularMedia(@c.tipo_consulta)
	  redirect_to atendimento_path, notice: "Duração: #{@c.fim.min - @c.inicio.min} minuto(s)"
  end

  def desmarcar
	  @c = Consulta.find(params[:consulta])
	  @c.cancelado!
	  redirect_to consulta_path(@c), notice: 'Consulta desmarcada com sucesso.'
  end

  # GET /consultas/1
  # GET /consultas/1.json
  def show
  end

  # GET /consultas/new
  def new
    @search_query = params[:q]
    @search_query = @search_query.strip if @search_query != nil
    @paciente = Paciente.search(@search_query)
    @consulta = Consulta.new
    if @paciente != nil
      @consulta.paciente_nome = @paciente.nome
    end

    if ListaConsultasHelper.getDate(Date.current).empty?
  	  @consulta.inicio = Time.new.change(hour: 8)
    elsif Consulta.where(data: Date.current).where(status: 0).last == nil
      @consulta.inicio = Time.current
  	else
  	  @consulta.inicio = Consulta.where(data: Date.current).where(status: 0).order(:inicio).last.fim
 	end
  end

  # GET /consultas/1/edit
  def edit
    @search_query = params[:q]
    @search_query = @search_query.strip if @search_query != nil
    @paciente = Paciente.search(@search_query)
    if @paciente != nil
      @consulta.paciente_nome = @paciente.nome
    end
  end

  # POST /consultas
  # POST /consultas.json
  def create
    @consulta = Consulta.new(consulta_params)
    @consulta.paciente_id = ConsultasHelper.getIdPaciente(@consulta.paciente_nome)
  	@consulta.tipo_consulta_id = params[:tipo_consulta][:tipo_consulta_id]
  	@consulta.duracao = ConsultasHelper.getDuracaoConsulta(@consulta.tipo_consulta_id)
  	@consulta.tipo_consulta = ConsultasHelper.getNomeTipoConsulta(@consulta.tipo_consulta_id)
	  @consulta.numeroFila = ConsultasHelper.getNumFila()
	  @consulta.fim = ConsultasHelper.getFimConsulta(@consulta)

    respond_to do |format|
      if @consulta.save
        format.html { redirect_to @consulta, notice: 'Consulta foi criada com sucesso.' }
        format.json { render :show, status: :created, location: @consulta }
      else
        format.html { render :new }
        format.json { render json: @consulta.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /consultas/1
  # PATCH/PUT /consultas/1.json
  def update
    @consulta.paciente_id = ConsultasHelper.getIdPaciente(@consulta.paciente_nome)
  	@consulta.tipo_consulta_id = params[:tipo_consulta][:tipo_consulta_id]
  	@consulta.duracao = ConsultasHelper.getDuracaoConsulta(@consulta.tipo_consulta_id)
  	@consulta.tipo_consulta = ConsultasHelper.getNomeTipoConsulta(@consulta.tipo_consulta_id)
	  @consulta.numeroFila = ConsultasHelper.getNumFila()
    respond_to do |format|
      if @consulta.update(consulta_params)
        @consulta.fim = ConsultasHelper.getFimConsulta(@consulta)
        @consulta.update(consulta_params)
        format.html { redirect_to @consulta, notice: 'Consulta foi atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @consulta }
      else
        format.html { render :edit }
        format.json { render json: @consulta.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consultas/1
  # DELETE /consultas/1.json
  def destroy
    @consulta.destroy
    respond_to do |format|
      format.html { redirect_to consultas_url, notice: 'Consulta foi apagada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consulta
      @consulta = Consulta.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def consulta_params
      params.require(:consulta).permit(:inicio, :fim, :duracao, :data, :status, :numeroFila, :paciente_nome, :tipo_consulta)
    end
end
