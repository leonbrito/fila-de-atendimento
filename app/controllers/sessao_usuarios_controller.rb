class SessaoUsuariosController < ApplicationController

    def new
        if (paciente_logado?)
            redirect_to paciente_consultas_path
        end
        @sessaousuario = SessaoUsuario.new(session)
    end

    def create
        @sessaousuario = SessaoUsuario.new(session, params[:sessao_usuario])
        if(@sessaousuario.authenticate!)
            if(usuario_atual.nivel == "auxiliar")
                redirect_to lista_consultas_path, notice: "Logado, Bem Vindo  #{@sessaousuario.nome}"
            else
                redirect_to atendimento_path, notice: "Logado, Bem Vindo  #{@sessaousuario.nome}"
            end
        else
            render :new, notice: "Errou"
        end
    end

    def destroy
        sessao_usuario.destroy
        redirect_to root_path, notice: "Logout efetuado"
    end

end